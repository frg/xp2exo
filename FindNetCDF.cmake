FIND_PATH(NetCDF_INCLUDE_DIR netcdf.h)
FIND_LIBRARY(NetCDF_LIBRARIES
             NAMES netcdf
             ${NetCDF_PREFIX}
             ${NetCDF_PREFIX}/lib64
             ${NetCDF_PREFIX}/lib
             /usr/local/lib64
             /usr/lib64
             /usr/lib64/netcdf-3
             /usr/local/lib
             /usr/lib
             /usr/lib/netcdf-3)
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(NetCDF DEFAULT_MSG NetCDF_INCLUDE_DIR NetCDF_LIBRARIES)
MARK_AS_ADVANCED(NetCDF_INCLUDE_DIR NetCDF_LIBRARIES)
