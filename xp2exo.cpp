//-----------------------------------------------------------
//  Description: This routine extracts a mesh and results in
//               XPost format and converts into Exodus format
//               which can then be loaded in Paraview.
//       Inputs: <path to mesh file> <path/s to result file>
//      Outputs: <path to file containing mesh and results>
//       Author: Alex Main
//        Notes: (WARNING) this code doesn't verify inputs.
//-----------------------------------------------------------

/*
  Exodus Naming Conventions
  http://www.paraview.org/Wiki/Restarted_Simulation_Readers#Exodus_Naming_Conventions

  By default, ParaView will assume that the numbers at the end of a file
  represent partition numbers and will attempt to load them all at once. But
  there is a convention for having numbers specifying a temporal sequence instead.
  If the files end in .e-s.{RS} (where {RS} is some number sequence), then the
  numbers (called restart numbers) are taken as a partition in time instead of
  space. For example, the following sequence of files represents a time series:

  mysimoutput.e-s.000
  mysimoutput.e-s.001
  mysimoutput.e-s.002
*/

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>
#include <sstream>
#include <cstring>
#include <set>
#include <list>
#include <limits>
#include "netcdf.h"
#include "exodusII.h"

using namespace std;

struct elem_block {
  int elid;
  int num_elem;
  list<int> elems;
  vector<int> *connect;
  string name;
};

typedef map<int, elem_block> top_block;

void writeMesh(int, int, int, int, map<int, int>&, vector<double>&, vector<double>&,
               vector<double>&, vector<top_block *>&, set<int>&);
void writeGlobalVar(int, int , char **, int *);
void deleteElements(int &, vector<top_block *>&, set<int>&, map<int, int>&);

static void usage() {
  cout << "Usage: xp2exo <top file> <output file (exodus ii)> [<xpost result list>]" << endl;
}

int main(int argc, char **argv) {
  string word1,word2;
  int int1,ncomponent,frame;

  map<string, int> elem_block_id;
  vector<top_block *> myElements;
  set<int> deletedElements;

  double x,y,z;
  double garbage;
  char line[512];
  int CPU_word_size=8,IO_word_size=8;
  int bFictiousTime = 0;

  map<int, int> exodusRemap;

  int no_remap = 0;

  if (argc < 3) {
    usage();
    return 0;
  }

  if (strcmp(argv[1], "--noremap") == 0) {
    no_remap = 1;
    argc--;
    argv = argv+1;
  }

  //parsing
  ifstream inMesh(argv[1],ios::in);
  if (!inMesh.is_open()) {
    cerr << "Error: could not read file " << argv[1] << endl;
    exit(-1);
  }

  //preparation
  while(!inMesh.eof()) {
    inMesh.getline(line,512);
    stringstream mystrm(line);
    mystrm >> word1;
    if (word1 == "//") continue;
    mystrm >> word2;
    if (word1 != "Nodes") {
      cerr << "Error: could not read nodes\n";
      exit(-1);
    }
    break;
  }

  int exoid = ex_create(argv[2],        // filename path
                        EX_CLOBBER,     // create mode
                        &CPU_word_size, // CPU float word size in bytes
                        &IO_word_size); // I/O float word size in bytes

  int num_dim = 3;
  vector<double> X,Y,Z;

  //load node coordinates
  while(!inMesh.eof()) {
    inMesh >> int1;
    if (inMesh.fail()) {
      inMesh.clear();
      break;
    }
    inMesh >> x >> y >> z;
    X.push_back(x);
    Y.push_back(y);
    Z.push_back(z);
    exodusRemap[ int1 ] = X.size();
  }

  int nNodes = (int)X.size();
  cout << "Loaded "<< nNodes <<" nodes" << endl;

  string name,tmp,tmp1;
  int id,elid,i,en;

  int num_elem_block = 0;
  int num_elem = 0;
  map<int, int> elem_nodes;
  elem_nodes[1] = 2;
  elem_nodes[2] = 4;
  elem_nodes[4] = 3;
  elem_nodes[5] = 4;
  elem_nodes[6] = 2;
  elem_nodes[66] = 2;
  elem_nodes[101] = 2;
  elem_nodes[103] = 4;
  elem_nodes[106] = 2;
  elem_nodes[107] = 2;
  elem_nodes[117] = 8;
  elem_nodes[121] = 2;
  elem_nodes[122] = 2;
  elem_nodes[102] = 4;
  elem_nodes[104] = 3;
  elem_nodes[108] = 3;
  elem_nodes[123] = 4;
  elem_nodes[124] = 6;
  elem_nodes[120] = 3;
  elem_nodes[125] = 10;
  elem_nodes[172] = 20;
  elem_nodes[188] = 4;
  elem_nodes[141] = 4;
  elem_nodes[145] = 8;
  elem_nodes[146] = 3;
  elem_nodes[506] = 2;
  elem_nodes[2120] = 4;
  elem_nodes[4746] = 4;
  elem_nodes[150] = 4;
  elem_nodes[151] = 8;
  elem_nodes[191] = 32;
  elem_nodes[195] = 64;
  elem_nodes[130] = 4;
  elem_nodes[132] = 8;
  elem_nodes[135] = 3;
  elem_nodes[138] = 6;
  elem_nodes[149] = 3;
  elem_nodes[164] = 20;
  elem_nodes[210] = 8;

  while(!inMesh.eof()) {
    inMesh.getline(line,512);
    stringstream mystrm(line);
    mystrm >> tmp1;
    if (tmp1 == "//") continue;
    if (tmp1 != "Elements")
      break;
    mystrm >> name >> tmp >> tmp;

    top_block* top_blocks;
    map<string,int>::iterator it = elem_block_id.find(name);
    if (it == elem_block_id.end()) {
      top_blocks = new top_block;
      elem_block_id.insert(pair<string,int>(name, myElements.size()));
      myElements.push_back(top_blocks);
    }
    else {
      top_blocks = myElements[it->second];
    }
    elem_block* curr;

    while(!inMesh.eof()) {
      inMesh >> id;
      if (inMesh.fail()) {
        if (!inMesh.eof())
          inMesh.clear();
        break;
      }

      inMesh >> en;
      if (elem_nodes.find(en) == elem_nodes.end()) {
        cerr << "Error: unknown topology " << en << endl;
        exit(-1);
      }

      top_block::iterator itr = top_blocks->find(en);
      if (itr != top_blocks->end()) {
        curr = &itr->second;
      }
      else {
        elem_block E;
        E.elid = en;
        E.name = name;
        E.num_elem = 0;
        E.connect = new vector<int>;
        curr = &((*top_blocks)[en] = E);
      }

      curr->elems.push_back(id);
      int nn = elem_nodes[curr->elid];
      for (i = 0; i < nn; ++i) {
        inMesh >> en;
        curr->connect->push_back( exodusRemap[en] );
      }
      if (curr->elid == 172 || curr->elid == 164) {
        // if element type is 172 need to switch nodes 12-15 with nodes 16-19
        swap_ranges(curr->connect->end()-4, curr->connect->end(), curr->connect->end()-8);
      }
      else if (curr->elid == 191) {
        // if element type is 191 need to switch nodes 16-23 with nodes 24-31
        swap_ranges(curr->connect->end()-8, curr->connect->end(), curr->connect->end()-16);
        // also need to reorder 16,17,18,19,20,21,22,23 -> 16,18,20,22,17,19,21,23
        vector<int>& connect = *(curr->connect);
        vector<int> connect_copy(32);
        std::copy(curr->connect->end()-32, curr->connect->end(), connect_copy.begin());
        connect[connect.size() - 32 + 16] = connect_copy[16];
        connect[connect.size() - 32 + 17] = connect_copy[18];
        connect[connect.size() - 32 + 18] = connect_copy[20];
        connect[connect.size() - 32 + 19] = connect_copy[22];
        connect[connect.size() - 32 + 20] = connect_copy[17];
        connect[connect.size() - 32 + 21] = connect_copy[19];
        connect[connect.size() - 32 + 22] = connect_copy[21];
        connect[connect.size() - 32 + 23] = connect_copy[23];
      }
      else if (curr->elid == 195) {
        int renum[64] = { 1, 9, 10, 2, 16, 33, 34, 11, 15, 36, 35, 12, 4, 14, 13, 3,
                          17, 37, 38, 18, 44, 45, 46, 39, 43, 48, 47, 40, 20, 42, 41, 19,
                          21, 49, 50, 22, 56, 57, 58, 51, 55, 60, 59, 52, 24, 54, 53, 23,
                          5, 25, 26, 6, 32, 61, 62, 27, 31, 64, 63, 28, 8, 30, 29, 7};
        vector<int>& connect = *(curr->connect);
        vector<int> connect_copy(64);
        std::copy(curr->connect->end()-64, curr->connect->end(), connect_copy.begin());
        for (i = 0; i < 64; ++i) {
          connect[connect.size() - 64 + renum[i] - 1] = connect_copy[i];
        }
      }
      else if (curr->elid == 210) {
        std::swap(*(curr->connect->end()-6), *(curr->connect->end()-5));
        std::swap(*(curr->connect->end()-2), *(curr->connect->end()-1));
      }
      ++num_elem;
    }
  }

  for (int i = 0; i < myElements.size(); ++i)
    num_elem_block += myElements[i]->size();

  inMesh.close();

  writeMesh(exoid, nNodes, num_elem, num_elem_block, elem_nodes, X, Y, Z, myElements, deletedElements);

  int m = 3;
  ifstream *sols = new ifstream[argc-3];
  int *ncomp = new int[argc-3];
  char **names = new char *[(argc-3)*9];
  int numV = 0;
  bool *isNodal = new bool[argc-3];
  const char *xyz[3] = {"X","Y","Z"};
  const char *xyz2[8] = {"1","2","3","4","5","6","7","8"};
  const char *xyz3[9] = {"XX","XY","XZ","YX","YY","YZ","ZX","ZY","ZZ"};
  int *decomposition = 0;
  int *decsize;
  int num_elem_local;
  int deletedElementsFile = -1, deletedElementNo;
  double deletedElementTime = numeric_limits<double>::infinity();
  string cause;

  num_elem_local = 0;
  for (top_block::iterator itr = myElements[0]->begin(); itr != myElements[0]->end(); ++itr) {
    num_elem_local += itr->second.elems.size();
  }

  for (i = 0; i < (argc-3)*9; ++i)
    names[i] = new char[64];
  for (m = 0; m < argc-3; ++m) {
    sols[m].open(argv[m+3],ios::in);
    if (!sols[m].good()) {
      cerr << "Error: could not read file " << argv[m+3] << endl;
      exit(-1);
    }
    else {
      cout << "Reading file " << argv[m+3] << endl;
    }
    sols[m].getline(line,512);
    if (strcmp(line,"#  time   Element_no   Cause") == 0) {
      if (strcmp(argv[2]+(strlen(argv[2])-2),".e") != 0) {
        cerr << "Error: output file must have \".e\" extension\n";
        exit(-1);
      }
      string cmd = "rm -f "; cmd += argv[2]; cmd += "-s.*";
      int x = system(cmd.c_str());
      isNodal[m] = false;
      deletedElementsFile = m;
      sols[m] >> deletedElementTime >> deletedElementNo >> cause;
      continue;
    }
    char type[64], name[64];
    int read = sscanf(line,"%s %s",type,name);
    if (read > 0 && strcmp(type,"Decomposition") != 0) { // this is a solution file
      if (strcmp(type,"Scalar") == 0)
        ncomponent = 1;
      else if (strcmp(type,"Vector") == 0)
        ncomponent = 3;
      else if (strcmp(type,"Vector5") == 0)
        ncomponent = 5;
      else if (strcmp(type,"Vector6") == 0)
        ncomponent = 6;
      else if (strcmp(type,"Vector7") == 0)
        ncomponent = 7;
      else if (strcmp(type,"Vector8") == 0)
        ncomponent = 8;
      else if (strcmp(type,"Vector9") == 0)
        ncomponent = 9;
      else {
        cerr << "Error: unable to determine number of components in solution\n";
        exit(-1);
      }
      cout << "Reading variable " << name << " with " << ncomponent << " components" << endl;

      for (i = 0; i < ncomponent; ++i) {
        strcpy(names[numV+i],name);
      }
      if (ncomponent == 3) {
        for (i = 0; i < ncomponent; ++i) {
          strcat(names[numV+i],xyz[i]);
        }
      }
      else if(ncomponent > 3 && ncomponent < 9) {
        for (i = 0; i < ncomponent; ++i) {
          strcat(names[numV+i],xyz2[i]);
        }
      }
      else if(ncomponent == 9) {
        for (i = 0; i < ncomponent; ++i) {
          strcat(names[numV+i],xyz3[i]);
        }
      }
      numV += ncomponent;
      ncomp[m] = ncomponent;
      isNodal[m] = true;
    }
    else { // this is a mesh decomposition file
      isNodal[m] = false;
      if (read <= 0) { // fluid decomp file has blank first line, structure decomp file does not
        sols[m].getline(line,512);
      }
      cout << "Line = " << line << endl;
      int nparts;
      sols[m] >> nparts;
      cout << "# of elems (3D) = " << num_elem_local << endl;
      decomposition = new int[num_elem_local];
      decsize = new int[nparts];
      int tmp;
      cout << "Reading decomposition into " << nparts << " parts" << endl;
      for (int j = 0; j < nparts; ++j) {
        sols[m] >> decsize[j];
        for (int l = 0; l < decsize[j]; ++l) {
          sols[m] >> tmp;
          decomposition[tmp-1] = j+1;
        }
      }
    }
  }

  if (argc > 3) {
      //load solution file
      writeGlobalVar(exoid, numV, names, decomposition);
      double *Sol = new double[nNodes*numV];
      double timestamp,dummy;
      int k = 0, l;

      int m0 = 0;
      if (!isNodal[0])
        ++m0;

      int mynn, lid;
      for (m = 0; m < argc-3; ++m) {
        if (isNodal[m])
          sols[m] >> mynn;
      }
      int s = 0;
      while (m0 < argc-3 && !sols[m0].eof() && argc > 3) {
        for (m = 0; m < argc-3; ++m) {
          if (isNodal[m])
            sols[m] >> timestamp;
        }
        if (sols[m0].eof())
          break;

        if (timestamp > 0 && deletedElementsFile > -1) {
          // check for deleted elements up to the current timestamp
          deletedElements.clear();
          while (deletedElementTime <= timestamp) {
            deletedElements.insert(deletedElementNo);
            sols[deletedElementsFile] >> deletedElementTime >> deletedElementNo >> cause;
            if (sols[deletedElementsFile].eof()) deletedElementTime = numeric_limits<double>::infinity();
          }

          // if there are any elements to delete then open a new output file and increment s
          if (deletedElements.size() > 0) {
            deleteElements(num_elem, myElements, deletedElements, elem_nodes);

            char *filename = new char[strlen(argv[2])+10];
            char extension[14];
            sprintf(extension,"-s.%04d",s+1);
            strcpy(filename,argv[2]);
            strcat(filename,extension);
            exoid = ex_create(filename,       // filename path
                              EX_CLOBBER,     // create mode
                              &CPU_word_size, // CPU float word size in bytes
                              &IO_word_size); // I/O float word size in bytes
            delete [] filename;

            writeMesh(exoid, nNodes, num_elem, num_elem_block, elem_nodes, X, Y, Z, myElements, deletedElements);
            writeGlobalVar(exoid, numV, names, decomposition);
            k = 0;
            s++;
          }
          cout << "Read part " << s << " frame " << k << " Timestamp = " << timestamp << endl;
        }
        else {
          cout << "Read frame " << k << " Timestamp = " << timestamp << endl;
        }

        ex_put_time(exoid, k+1, &timestamp);
        if (decomposition) {
          int elem_blk_id = 1;
          for (top_block::iterator itr = myElements[0]->begin(); itr != myElements[0]->end(); ++itr) {
            int num_elem_this_blk = itr->second.elems.size();
            double *decToWrite = new double[num_elem_this_blk];
            l = 0;
            for (list<int>::iterator itr2 = itr->second.elems.begin(); itr2 != itr->second.elems.end(); ++itr2) {
              decToWrite[l++] = (double) decomposition[*itr2-1];
            }
            ex_put_var(exoid, k+1, EX_ELEM_BLOCK, 1, elem_blk_id++, num_elem_this_blk, decToWrite);
            delete [] decToWrite;
          }
        }

        for (int i = 0; i < mynn; ++i) {
          l = 0;
          if (!no_remap && exodusRemap.find(i+1) == exodusRemap.end()) {
            for (m = 0; m < argc-3; ++m) {
              if (isNodal[m]) {
                for (int j = 0; j < ncomp[m]; ++j)
                  sols[m] >> dummy;
              }
            }
          }
          else {
            if (!no_remap)
          lid = exodusRemap[i+1];
            else
              lid = i+1;
            for (m = 0; m < argc-3; ++m) {
              if (isNodal[m]) {
                for (int j = 0; j < ncomp[m]; ++j, ++l)
                  sols[m] >> Sol[lid-1+nNodes*l];
              }
            }
          }
        }

        l = 0;
        for (m = 0; m < argc-3; ++m) {
          if (isNodal[m]) {
            for (int j = 0; j < ncomp[m]; ++j, ++l)
              ex_put_var(exoid, k+1, EX_NODAL, l+1, 0, nNodes, Sol+l*nNodes);
          }
        }

        ex_update(exoid);
        ++k;
      }
      if (m0 == 1 && argc == 4) { // decomposition only with no result fules
        if (decomposition) {
          int elem_blk_id = 1;
          for (top_block::iterator itr = myElements[0]->begin(); itr != myElements[0]->end(); ++itr) {
            int num_elem_this_blk = itr->second.elems.size();
            double *decToWrite = new double[num_elem_this_blk];
            l = 0;
            for (list<int>::iterator itr2 = itr->second.elems.begin(); itr2 != itr->second.elems.end(); ++itr2) {
              decToWrite[l++] = (double) decomposition[*itr2-1];
            }
            ex_put_var(exoid, k+1, EX_ELEM_BLOCK, 1, elem_blk_id++, num_elem_this_blk, decToWrite);
            delete [] decToWrite;
          }
        }
      }
      delete [] Sol;
  }
  // clean-up
  ex_close(exoid);

  delete [] sols;
  delete [] ncomp;
  for (i = 0; i < (argc-3)*6; ++i)
    delete [] names[i];
  delete [] names;
  delete [] isNodal;

  return 0;
}

void writeMesh(int exoid, int nNodes, int num_elem, int num_elem_block, map<int, int>& elem_nodes,
               vector<double>& X, vector<double>& Y, vector<double>& Z, vector<top_block *>& myElements,
               set<int>& deletedElements) {
  ex_put_init(exoid, "xp2exo",
              3,      // three dimensions
              nNodes, // number of nodes
              num_elem-deletedElements.size(), num_elem_block, 0, 0);

  char *coord_names[] = {"xcoor","ycoor","zcoor"};

  ex_put_coord(exoid, &(X[0]), &(Y[0]), &(Z[0]));
  ex_put_coord_names(exoid, coord_names);

  map<int, const char *> elem_types;
  elem_types[1] = "BEAM";
  elem_types[2] = "QUAD";
  elem_types[4] = "TRIANGLE";
  elem_types[5] = "TETRA";
  elem_types[6] = "BEAM";
  elem_types[66] = "BEAM";
  elem_types[101] = "BEAM";
  elem_types[103] = "QUAD";
  elem_types[106] = "BEAM";
  elem_types[107] = "BEAM";
  elem_types[102] = "QUAD";
  elem_types[104] = "TRIANGLE";
  elem_types[108] = "TRIANGLE";
  elem_types[117] = "HEX";
  elem_types[188] = "QUAD";
  elem_types[123] = "TETRA";
  elem_types[124] = "WEDGE";
  elem_types[125] = "TETRA";
  elem_types[141] = "TETRA";
  elem_types[145] = "HEX";
  elem_types[146] = "TRIANGLE";
  elem_types[172] = "HEX";
  elem_types[4746] = "QUAD";
  elem_types[506] = "BEAM";
  elem_types[122] = "BEAM";
  elem_types[121] = "BEAM";
  elem_types[120] = "TRIANGLE";
  elem_types[2120] = "QUAD";
  elem_types[150] = "TETRA";
  elem_types[151] = "HEX";
  elem_types[191] = "HEX";
  elem_types[195] = "HEX";
  elem_types[130] = "QUAD";
  elem_types[132] = "QUAD";
  elem_types[135] = "TRIANGLE";
  elem_types[138] = "TRIANGLE";
  elem_types[149] = "TRIANGLE";
  elem_types[164] = "HEX";
  elem_types[210] = "HEX";
  int i = 0;
  int bc = 0;
  for (i = 0; i < myElements.size(); ++i) {
    top_block& tb = *myElements[i];
    for (top_block::iterator itr = tb.begin(); itr != tb.end(); ++itr) {
      int nn = elem_nodes[itr->second.elid];
      ex_put_block(exoid, EX_ELEM_BLOCK, bc+1, elem_types[itr->second.elid],
                   itr->second.connect->size()/(nn), nn, 0, 0, 0);
      ex_put_conn(exoid, EX_ELEM_BLOCK, bc+1, &((*(itr->second.connect))[0]), 0, 0);
      ++bc;
    }
  }

  char **elem_block_names = new char *[bc];
  bc = 0;
  for (i = 0; i < myElements.size(); ++i) {
    top_block& tb = *myElements[i];
    for (top_block::iterator itr = tb.begin(); itr != tb.end(); ++itr) {
      elem_block_names[bc] = new char[strlen(itr->second.name.c_str())+strlen(elem_types[itr->second.elid])+2];
      sprintf(elem_block_names[bc],"%s#%s",itr->second.name.c_str(),elem_types[itr->second.elid]);
      ++bc;
    }
  }
  ex_put_names(exoid, EX_ELEM_BLOCK, elem_block_names);
  for (int i = 0; i < bc; ++i) delete [] elem_block_names[i];
  delete [] elem_block_names;
}

void writeGlobalVar(int exoid, int numV, char **names, int *decomposition) {
  ex_put_variable_param(exoid, EX_NODAL, numV);
  ex_put_variable_names(exoid, EX_NODAL, numV, names);
  if (decomposition) {
    char *dec_var_name[] = {"decomposition"};
    ex_put_variable_param(exoid, EX_ELEM_BLOCK, 1);
    ex_put_variable_names(exoid, EX_ELEM_BLOCK, 1, dec_var_name);
  }
}

void deleteElements(int &num_elem, vector<top_block *>& myElements, set<int>& deletedElements,
                    map<int, int>& elem_nodes) {
  // delete elements from the topology element set, and delete faces of deleted elements from the surface topologies
  for (int i = 0; i < 1; ++i) {
    top_block& tb = *myElements[i];
    for (top_block::iterator itr = tb.begin(); itr != tb.end(); ++itr) {
      int nn = elem_nodes[itr->second.elid];
      list<int>::iterator itr2 = itr->second.elems.begin();
      vector<int>::iterator itr3 = itr->second.connect->begin();
      while(itr2 != itr->second.elems.end()) {
        if (deletedElements.find(*itr2) != deletedElements.end()) {
          cerr << "Deleting element " << *itr2 << " from " << itr->second.name << endl;
#ifdef USE_CPP11
          for (int j = 1; j < myElements.size(); ++j) {
            top_block& fb = *myElements[j];
            for (top_block::iterator itr4 = fb.begin(); itr4 != fb.end(); ++itr4) {
              if (itr4->second.name.compare(0,8,"surface_") != 0) continue;
              int fnn = elem_nodes[itr4->second.elid];
              list<int>::iterator itr5 = itr4->second.elems.begin();
              vector<int>::iterator itr6 = itr4->second.connect->begin();
              while(itr5 != itr4->second.elems.end()) {
                if (all_of(itr6, itr6+fnn, [&](int k) { return (find(itr3, itr3+nn, k) != itr3+nn); })) {
                  cerr << "Deleting face " << *itr5 << " from " << itr4->second.name << endl;
                  itr5 = itr4->second.elems.erase(itr5);
                  itr6 = itr4->second.connect->erase(itr6, itr6+fnn);
                  num_elem--;
                }
                else {
                 itr5++;
                 itr6 += fnn;
                }
              }
            }
          }
#endif
          itr2 = itr->second.elems.erase(itr2);
          itr3 = itr->second.connect->erase(itr3, itr3+nn);
        }
        else {
          itr2++;
          itr3 += nn;
        }
      }
    }
  }
  num_elem -= deletedElements.size();
}
